package com.hendisantika.springformmailer.controller

import com.hendisantika.springformmailer.domain.FormBody
import com.hendisantika.springformmailer.mailgun.MailgunClient
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import javax.validation.Valid

/**
 * Created by IntelliJ IDEA.
 * Project : spring-form-mailer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/04/21
 * Time: 08.40
 */
@Controller
class IndexController(private val mailgunClient: MailgunClient) {
    @GetMapping("")
    fun index(formBody: FormBody) = "index"

    @PostMapping("/submit")
    fun submit(@Valid formBody: FormBody, bindingResult: BindingResult): String {
        if (bindingResult.hasErrors())
            return "index"

        return if (mailgunClient.sendEmail(formBody.toMailgunPayload())) "success" else "error"
    }
}