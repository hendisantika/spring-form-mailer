package com.hendisantika.springformmailer.mailgun

/**
 * Created by IntelliJ IDEA.
 * Project : spring-form-mailer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/04/21
 * Time: 07.54
 */
data class MailgunPayload(
    val name: String,
    val amountOfAdditionalAdults: Int,
    val amountOfAdditionalChildren: Int,
    val comments: String
)
