package com.hendisantika.springformmailer.domain

import com.hendisantika.springformmailer.mailgun.MailgunPayload
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : spring-form-mailer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/04/21
 * Time: 08.37
 */
data class FormBody(
    @field:NotBlank
    var name: String = "",

    @field:Min(0)
    var amountOfAdditionalAdults: Int = 0,

    @field:Min(0)
    var amountOfAdditionalChildren: Int = 0,

    var comments: String = ""
) {
    fun toMailgunPayload() = MailgunPayload(name, amountOfAdditionalAdults, amountOfAdditionalChildren, comments)
}
